package com.gitlab.edersix.serviceswebapp.data;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gitlab.edersix.serviceswebapp.modules.StaffMember;
// #step2
public interface StaffRepository extends JpaRepository<StaffMember, Long>{

}
