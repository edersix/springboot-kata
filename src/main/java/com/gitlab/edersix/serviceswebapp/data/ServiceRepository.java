package com.gitlab.edersix.serviceswebapp.data;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gitlab.edersix.serviceswebapp.modules.ServiceModel;

// the model and the data type of the id
public interface ServiceRepository extends JpaRepository<ServiceModel, Long>{

}
