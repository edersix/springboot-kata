package com.gitlab.edersix.serviceswebapp.service;

//import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.gitlab.edersix.serviceswebapp.data.ServiceRepository;
import com.gitlab.edersix.serviceswebapp.modules.ServiceModel;

@Service
public class ServiceServicce {
// Commented when repository was implemented
//	private static final List<ServiceModel> services= new ArrayList<ServiceModel>();
//	
//	static {
//		for(int i=0;i<10;i++) {
//			services.add(new ServiceModel(i,"#"+i,"service_"+i,"/service_"+i));
//		}
//	}
	
	private final ServiceRepository servRepository;
	
	
	public ServiceServicce(ServiceRepository servRepository) {
//		super();
		this.servRepository = servRepository;
	}


	public List<ServiceModel> getAllServices() {
		
//		return services;
		return servRepository.findAll();
	}
}
