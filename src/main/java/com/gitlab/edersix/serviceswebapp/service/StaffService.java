package com.gitlab.edersix.serviceswebapp.service;

//import java.util.ArrayList;
import java.util.List;
//import java.util.UUID;

import org.springframework.stereotype.Service;

import com.gitlab.edersix.serviceswebapp.data.StaffRepository;
//import com.gitlab.edersix.serviceswebapp.modules.Position;
import com.gitlab.edersix.serviceswebapp.modules.StaffMember;

@Service
public class StaffService {
//private static final List<StaffMember> staff= new ArrayList<>();

//	static {
//
//		staff.add(new StaffMember(UUID.randomUUID().toString(),"Eder","Cruz",Position.SR_DEVELOPER));
//		staff.add(new StaffMember(UUID.randomUUID().toString(),"Paulina","Luna",Position.TEAM_LEADER));
//		staff.add(new StaffMember(UUID.randomUUID().toString(),"Diego","Cruz",Position.PROJECT_MANAGER));
//		staff.add(new StaffMember(UUID.randomUUID().toString(),"Camilo","Cruz",Position.MID_DEVELOPER));
//		staff.add(new StaffMember(UUID.randomUUID().toString(),"Peptida","Cruz",Position.JR_DEVELOPER));
//		staff.add(new StaffMember(UUID.randomUUID().toString(),"Revillagijedo","Cruz",Position.JR_DEVELOPER));
//		staff.add(new StaffMember(UUID.randomUUID().toString(),"Squiggly","Cruz",Position.JR_DEVELOPER));
//		staff.add(new StaffMember(UUID.randomUUID().toString(),"Melocoton","Cruz",Position.JR_DEVELOPER));
//		
//	}
//	// #step3
	private final StaffRepository staffReposiroty;
	
	
	public StaffService(StaffRepository staffReposiroty) {
//	super();
	this.staffReposiroty = staffReposiroty;
}


	public List<StaffMember> getAllStaff() {
		
		return staffReposiroty.findAll();
	}
}
