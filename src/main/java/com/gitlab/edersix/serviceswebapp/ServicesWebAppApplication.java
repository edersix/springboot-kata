package com.gitlab.edersix.serviceswebapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicesWebAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicesWebAppApplication.class, args);
	}

}
