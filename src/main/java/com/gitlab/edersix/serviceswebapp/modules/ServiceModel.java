package com.gitlab.edersix.serviceswebapp.modules;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SERVICES")
public class ServiceModel {
	@Id
	@Column(name="SERVICE_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	@Column(name="SERVICE_NUMBER")
	private String number;
	@Column(name="NAME")
	private String name;
	@Column(name="URI")
	private String uri;
	
	
	public ServiceModel() {
		super();
	}


	public ServiceModel(long id, String number, String name, String uri) {
		super();
		this.id = id;
		this.number = number;
		this.name = name;
		this.uri = uri;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getNumber() {
		return number;
	}


	public void setNumber(String number) {
		this.number = number;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getUri() {
		return uri;
	}


	public void setUri(String uri) {
		this.uri = uri;
	}

	
}
