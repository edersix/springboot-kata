package com.gitlab.edersix.serviceswebapp.modules;

public enum Position {
	JR_DEVELOPER, MID_DEVELOPER, SR_DEVELOPER, TEAM_LEADER, PROJECT_MANAGER;
	
	public String toString() {
		switch (this) {
		case JR_DEVELOPER:
			return "Jr Developer";
		case MID_DEVELOPER:
			return "Jr Developer";
		case PROJECT_MANAGER:
			return "Project Manager";
		case SR_DEVELOPER:
			return "Sr Developer";
		case TEAM_LEADER:
			return "Team LEader";
		default:
			return "";
		}
	}
}
