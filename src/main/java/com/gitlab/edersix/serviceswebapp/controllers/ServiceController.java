package com.gitlab.edersix.serviceswebapp.controllers;

//import java.util.ArrayList;
//import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

//import com.gitlab.edersix.serviceswebapp.modules.ServiceModel;
import com.gitlab.edersix.serviceswebapp.service.ServiceServicce;



@Controller
@RequestMapping("/services")
public class ServiceController {
	
//	private static final List<ServiceModel> services= new ArrayList<ServiceModel>();
	@Autowired
	private final ServiceServicce servService;
	
	
//	static {
//		for(int i=0;i<10;i++) {
//			services.add(new ServiceModel(i,"#"+i,"service_"+i,"/service_"+i));
//		}
//	}
	public ServiceController(ServiceServicce servService) {
		super();
		this.servService = servService;
	}
	
	@GetMapping
	public String getAllServices(Model model) {
//		model.addAttribute("services", services);
		model.addAttribute("services", servService.getAllServices());
		return "services";//name of the view
	}



	
}
