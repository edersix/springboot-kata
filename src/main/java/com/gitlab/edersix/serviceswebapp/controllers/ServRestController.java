package com.gitlab.edersix.serviceswebapp.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.edersix.serviceswebapp.modules.ServiceModel;
import com.gitlab.edersix.serviceswebapp.service.ServiceServicce;

@RestController
@RequestMapping("/api/services")
public class ServRestController {
	
	private final ServiceServicce servService;

	public ServRestController(ServiceServicce servService) {
//		super();
		this.servService = servService;
	}
	
	@GetMapping
	public List<ServiceModel> getAllServices()
	{
		return servService.getAllServices();
	}
}
