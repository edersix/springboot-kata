package com.gitlab.edersix.serviceswebapp.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.edersix.serviceswebapp.modules.StaffMember;
import com.gitlab.edersix.serviceswebapp.service.StaffService;

@RestController
@RequestMapping("/api/staff")
public class StaffRestController {
	
	private final StaffService staff;

	public StaffRestController(StaffService staff) {
		super();
		this.staff = staff;
	}
	
	@GetMapping
	public List<StaffMember> getAllStaff(){
		return staff.getAllStaff();
	}
	

}
